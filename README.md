# smart-alipay
支付宝支付工具类

## 项目结构
```shell
├── alipay
│   ├── src/main
│   ├──├──java/com/cppba/alipay
│   ├──├──├──base                   // 基础类
│   ├──├──├──bizcontent             // 业务参数类
│   ├──├──├──request                // 请求调用类
│   ├──├──├──response               // 结果接收类
│   ├──├──├──util                   // 工具类
├──sample
│   ├── src/main
│   ├──├──java/com/cppba/alipay
│   ├──├──├──controller             // 使用demo类
│   ├──├──├──properties             // 自定义参数类
│   ├──├──resources                 // 自定义配置文件
```

## TODO LIST:
### 支付API
* [x] [电脑网站支付(alipay.trade.page.pay)](sample/src/main/java/com/cppba/alipay/controller/AlipayTradePagePayController.java)
* [x] [手机网站支付(alipay.trade.wap.pay)](sample/src/main/java/com/cppba/alipay/controller/AlipayTradeWapPayController.java)
* [x] [App支付(alipay.trade.app.pay)](sample/src/main/java/com/cppba/alipay/controller/AlipayTradeAppPayController.java)
* [x] [交易查询(alipay.trade.query)](sample/src/main/java/com/cppba/alipay/controller/AlipayTradeQueryController.java)
* [x] [统一收单交易退款接口(alipay.trade.refund)](sample/src/main/java/com/cppba/alipay/controller/AlipayTradeRefundController.java)
* [x] [统一收单线下交易预创建(alipay.trade.precreate)](sample/src/main/java/com/cppba/alipay/controller/AlipayTradePrecreateController.java)
* [x] [单笔转账到支付宝账户接口(alipay.fund.trans.toaccount.transfer)](sample/src/main/java/com/cppba/alipay/controller/AlipayFundTransToaccountTransferController.java)
* [x] [查询对账单下载地址(alipay.data.dataservice.bill.downloadurl.query)](sample/src/main/java/com/cppba/alipay/controller/AlipayDataDataserviceBillDownloadurlQueryController.java)
* [ ] 统一收单交易退款查询(alipay.trade.fastpay.refund.query)

### 第三方应用授权
* [ ] 换取商户授权令牌(alipay.open.auth.token.app)
* [ ] 查询授权信息(alipay.open.auth.token.app.query)
* [ ] 更多接口未完待续。。。

## 简介
### 调用简单，demo简明扼要
![demo](https://images.gitee.com/uploads/images/2019/0911/142719_0e5b3152_142518.jpeg)
![统一收单线下交易预创建](https://images.gitee.com/uploads/images/2019/0911/142719_12d803e4_142518.jpeg)

### 返回结果清晰
![成功返回](https://images.gitee.com/uploads/images/2019/0911/142719_f824948f_142518.jpeg)
![失败返回](https://images.gitee.com/uploads/images/2019/0911/142720_b8ee1003_142518.jpeg)


### 注释丰富，拓展方便
![拓展接口](https://images.gitee.com/uploads/images/2019/0911/142719_2f9b8162_142518.jpeg)

