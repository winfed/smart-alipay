package com.cppba.alipay.bizcontent;

import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.cppba.alipay.base.response.AlipayResponse;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 业务请求参数的集合
 *
 * @author winfed
 * @create 2017-11-21 9:57
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlipayTradeRefundBizContent extends AlipayBizContent {


    /**
     * 商家订单号
     * 6823789339978248
     */
    @JsonProperty("out_trade_no")
    private String outTradeNo;

    /**
     * 支付宝交易号
     * 2013112011001004330000121536
     */
    @JsonProperty("trade_no")
    private String tradeNo;

    /**
     * 需要退款的金额，该金额不能大于订单金额,单位为元，支持两位小数
     * 200.12
     */
    @JsonProperty("refund_amount")
    private String refundAmount;

    /**
     * 退款的原因说明
     * 正常退款
     */
    @JsonProperty("refund_reason")
    private String refundReason;

    /**
     * 标识一次退款请求，同一笔交易多次退款需要保证唯一，如需部分退款，则此参数必传。
     * HZ01RF001
     */
    @JsonProperty("out_request_no")
    private String outRequestNo;

    /**
     * 商户的操作员编号
     * OP001
     */
    @JsonProperty("operator_id")
    private String operatorId;

    /**
     * 商户门店编号
     * NJ_S_001
     */
    @JsonProperty("store_id")
    private String storeId;

    /**
     * 商户机具终端编号
     * NJ_T_001
     */
    @JsonProperty("terminal_id")
    private String terminalId;
}