package com.cppba.alipay.bizcontent;

import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 查询对账单下载地址
 * 接口名称：alipay.data.dataservice.bill.downloadurl.query
 * url https://docs.open.alipay.com/api_15/alipay.data.dataservice.bill.downloadurl.query
 * @author winfed
 * @create 2018-11-10 16:57
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlipayDataDataserviceBillDownloadurlQueryBizContent extends AlipayBizContent {

	/**
	 * 账单类型
	 * bill_type
	 * 账单类型，商户通过接口或商户经开放平台授权后其所属服务商通过接口可以获取以下账单类型：trade、signcustomer；trade指商户基于支付宝交易收单的业务账单；signcustomer是指基于商户支付宝余额收入及支出等资金变动的帐务账单；
	 */
	@JsonProperty("bill_type")
	private String billType;

	/**
	 * 账单时间
	 * bill_date
	 * 账单时间：日账单格式为yyyy-MM-dd，月账单格式为yyyy-MM。
	 */
	@JsonProperty("bill_date")
	private String billDate;
}