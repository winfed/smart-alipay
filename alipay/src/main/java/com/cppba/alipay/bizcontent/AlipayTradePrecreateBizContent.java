package com.cppba.alipay.bizcontent;

import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * 业务请求参数的集合
 *
 * @author winfed
 * @create 2017-10-23 9:57
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlipayTradePrecreateBizContent extends AlipayBizContent {
    /**
     * 必填
     * 商户订单号，64个字符以内、可包含字母、数字、下划线；需保证在商户端不重复
     * 20150320010101001
     */
    @JsonProperty("out_trade_no")
    private String outTradeNo;

    /**
     * 卖家支付宝用户ID。 如果该值为空，则默认为商户签约账号对应的支付宝用户ID
     * 2088102146225135
     */
    @JsonProperty("seller_id")
    private String sellerId;

    /**
     * 必填
     * 订单总金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000]
     * 88.88
     */
    @JsonProperty("total_amount")
    private String totalAmount;

    /**
     * 可打折金额. 参与优惠计算的金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000] 如果该值未传入，但传入了【订单总金额】，【不可打折金额】则该值默认为【订单总金额】-【不可打折金额】
     * 8.88
     */
    @JsonProperty("discountable_amount")
    private String discountableAmount;

    /**
     * 必填
     * 订单标题
     * Iphone6 16G
     */
    @JsonProperty("subject")
    private String subject;

    /**
     * 订单包含的商品列表信息，Json格式： {"show_url":"https://或http://打头的商品的展示地址"} ，在支付时，可点击商品名称跳转到该地址
     * {"show_url":"https://www.alipay.com"}
     */
    @JsonProperty("goods_detail")
    private List<GoodsDetail> goodsDetail;

    /**
     * 订单描述
     * Iphone6 16G
     */
    @JsonProperty("body")
    private String body;

    /**
     * 商户操作员编号
     * yx_001
     */
    @JsonProperty("operator_id")
    private String operatorId;

    /**
     * 商户门店编号
     * NJ_001
     */
    @JsonProperty("store_id")
    private String storeId;

    /**
     * 禁用渠道，用户不可用指定渠道支付
     * 当有多个渠道时用“,”分隔
     * 注：与enable_pay_channels互斥
     * Iphone6 16G
     */
    @JsonProperty("disable_pay_channels")
    private String disablePayChannels;

    /**
     * 可用渠道，用户只能在指定渠道范围内支付
     * 当有多个渠道时用“,”分隔
     * 注：与disable_pay_channels互斥
     * pcredit,moneyFund,debitCardExpress
     */
    @JsonProperty("enable_pay_channels")
    private String enablePayChannels;

    /**
     * 商户机具终端编号
     * NJ_T_001
     */
    @JsonProperty("terminal_id")
    private String terminalId;

    /**
     * 业务扩展参数
     * {"sys_service_provider_id":"2088511833207846"}
     */
    @JsonProperty("extend_params")
    private ExtendParams extendParams;

    /**
     * （1c-当天的情况下，无论交易何时创建，都在0点关闭）。 该参数数值不接受小数点， 如 1.5h，可转换为 90m。该参数在请求到支付宝时开始计时。
     * 2h
     */
    @JsonProperty("timeout_express")
    private String timeoutExpress;

    /**
     * 商户传入业务信息，具体值要和支付宝约定，应用于安全，营销等参数直传场景，格式为json格式
     * {"data":"123"}
     */
    @JsonProperty("business_params")
    private String businessParams;

    /**
     * 业务扩展参数
     */
    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ExtendParams {
        /**
         * 系统商编号，该参数作为系统商返佣数据提取的依据，请填写系统商签约协议的PID
         * 2088511833207846
         */
        @JsonProperty("sys_service_provider_id")
        private String sysWerviceProviderId;
    }

    /**
     * 商品明细说明
     */
    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class GoodsDetail {
        /**
         * 商品的编号
         * apple-01
         */
        @JsonProperty("goods_id")
        private String goodsId;

        /**
         * 商品名称
         * ipad
         */
        @JsonProperty("goods_name")
        private String goodsName;

        /**
         * 商品数量
         * 1
         */
        @JsonProperty("quantity")
        private String quantity;

        /**
         * 商品单价，单位为元
         * 1000
         */
        @JsonProperty("price")
        private String price;

        /**
         * 商品类目
         * 34543238
         */
        @JsonProperty("goods_category")
        private String goodsCategory;

        /**
         * 商品描述信息
         * 特价手机
         */
        @JsonProperty("body")
        private String body;

        /**
         * 商品的展示地址
         * http://www.alipay.com/xxx.jpg
         */
        @JsonProperty("show_url")
        private String showUrl;
    }
}
