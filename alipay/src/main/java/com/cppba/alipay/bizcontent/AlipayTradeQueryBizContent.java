package com.cppba.alipay.bizcontent;

import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 业务请求参数的集合
 *
 * @author winfed
 * @create 2017-10-23 14:57
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlipayTradeQueryBizContent extends AlipayBizContent {

    /**
     * 订单支付时传入的商户订单号,和支付宝交易号不能同时为空。
     * trade_no,out_trade_no如果同时存在优先取trade_no
     * 20150320010101001
     */
    @JsonProperty("out_trade_no")
    private String outTradeNo;

    /**
     * 支付宝交易号，和商户订单号不能同时为空
     * 2014112611001004680073956707
     */
    @JsonProperty("trade_no")
    private String tradeNo;
}
