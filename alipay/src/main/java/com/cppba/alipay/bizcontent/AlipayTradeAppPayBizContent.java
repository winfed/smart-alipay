package com.cppba.alipay.bizcontent;

import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 业务请求参数的集合
 *
 * @author winfed
 * @create 2017-10-23 9:57
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlipayTradeAppPayBizContent extends AlipayBizContent {
    /**
     * 订单描述
     * Iphone6 16G
     */
    @JsonProperty("body")
    private String body;

    /**
     * 必填
     * 订单标题
     * Iphone6 16G
     */
    @JsonProperty("subject")
    private String subject;

    /**
     * 必填
     * 商户订单号，64个字符以内、可包含字母、数字、下划线；需保证在商户端不重复
     * 20150320010101001
     */
    @JsonProperty("out_trade_no")
    private String outTradeNo;

    /**
     * （1c-当天的情况下，无论交易何时创建，都在0点关闭）。 该参数数值不接受小数点， 如 1.5h，可转换为 90m。该参数在请求到支付宝时开始计时。
     * 2h
     */
    @JsonProperty("timeout_express")
    private String timeoutExpress;

    /**
     * 必填
     * 订单总金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000]
     * 88.88
     */
    @JsonProperty("total_amount")
    private String totalAmount;

    /**
     * 必填
     * 销售产品码，与支付宝签约的产品码名称。 注：目前仅支持QUICK_MSECURITY_PAY
     * FAST_INSTANT_TRADE_PAY
     */
    @JsonProperty("product_code")
    private String productCode = "QUICK_MSECURITY_PAY";

    /**
     * 商品主类型：0—虚拟类商品，1—实物类商品（默认）
     * 注：虚拟类商品不支持使用花呗渠道
     * 0
     */
    @JsonProperty("goods_type")
    private String goodsType;

    /**
     * 公用回传参数，如果请求时传递了该参数，则返回给商户时会回传该参数。支付宝只会在异步通知时将该参数原样返回。本参数必须进行UrlEncode之后才可以发送给支付宝
     * merchantBizType%3d3C%26merchantBizNo%3d2016010101111
     */
    @JsonProperty("passback_params")
    private String passbackParams;

    /**
     * 优惠参数
     * 注：仅与支付宝协商后可用
     * {"storeIdType":"1"}
     */
    @JsonProperty("promo_params")
    private String promoParams;

    /**
     * 业务扩展参数
     * {"sys_service_provider_id":"2088511833207846"}
     */
    @JsonProperty("extend_params")
    private ExtendParams extendParams;

    /**
     * 可用渠道，用户只能在指定渠道范围内支付
     * 当有多个渠道时用“,”分隔
     * 注：与disable_pay_channels互斥
     * pcredit,moneyFund,debitCardExpress
     */
    @JsonProperty("enable_pay_channels")
    private String enablePayChannels;

    /**
     * 禁用渠道，用户不可用指定渠道支付
     * 当有多个渠道时用“,”分隔
     * 注：与enable_pay_channels互斥
     * Iphone6 16G
     */
    @JsonProperty("disable_pay_channels")
    private String disablePayChannels;

    /**
     * 商户门店编号。该参数用于请求参数中以区分各门店，非必传项。
     * NJ_001
     */
    @JsonProperty("store_id")
    private String storeId;

    /**
     * 外部指定买家，详见外部用户ExtUserInfo参数说明
     */
    @JsonProperty("ext_user_info")
    private ExtUserInfo extUserInfo;

    /**
     * 业务扩展参数
     */
    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ExtendParams {
        /**
         * 系统商编号，该参数作为系统商返佣数据提取的依据，请填写系统商签约协议的PID
         * 2088511833207846
         */
        @JsonProperty("sys_service_provider_id")
        private String sysWerviceProviderId;

        /**
         * 是否发起实名校验
         * T：发起
         * F：不发起
         * F
         */
        @JsonProperty("needBuyerRealnamed")
        private String needBuyerRealnamed;

        /**
         * 账务备注
         * 注：该字段显示在离线账单的账务备注中
         * 促销
         */
        @JsonProperty("TRANS_MEMO")
        private String transMemo;

        /**
         * 花呗分期数（目前仅支持3、6、12）
         * 注：使用该参数需要仔细阅读“花呗分期接入文档”
         * 3
         */
        @JsonProperty("hb_fq_num")
        private String hbFqNum;

        /**
         * 卖家承担收费比例，商家承担手续费传入100，用户承担手续费传入0，仅支持传入100、0两种，其他比例暂不支持
         * 注：使用该参数需要仔细阅读“花呗分期接入文档”
         * 100
         */
        @JsonProperty("hb_fq_seller_percent")
        private String hbFqSellerPercent;
    }

    /**
     * 外部用户ExtUserInfo参数说明
     */
    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ExtUserInfo {
        /**
         * 姓名
         * 注： need_check_info=T时该参数才有效
         * 李明
         */
        @JsonProperty("name")
        private String name;

        /**
         * 手机号
         * 注：该参数暂不校验
         * 16587658765
         */
        @JsonProperty("mobile")
        private String mobile;

        /**
         * 身份证：IDENTITY_CARD、护照：PASSPORT、军官证：OFFICER_CARD、士兵证：SOLDIER_CARD、户口本：HOKOU等。如有其它类型需要支持，请与蚂蚁金服工作人员联系。
         * 注： need_check_info=T时该参数才有效
         * IDENTITY_CARD
         */
        @JsonProperty("cert_type")
        private String certType;

        /**
         * 证件号
         * 注：need_check_info=T时该参数才有效
         * 362334768769238881
         */
        @JsonProperty("cert_no")
        private String certNo;

        /**
         * 允许的最小买家年龄，买家年龄必须大于等于所传数值
         * 注：
         * 1. need_check_info=T时该参数才有效
         * 2. min_age为整数，必须大于等于0
         * 18
         */
        @JsonProperty("min_age")
        private String minAge;

        /**
         * 是否强制校验付款人身份信息
         * T:强制校验，F：不强制
         * F
         */
        @JsonProperty("fix_buyer")
        private String fixBuyer;

        /**
         * 是否强制校验身份信息
         * T:强制校验，F：不强制
         * F
         */
        @JsonProperty("need_check_info")
        private String needCheckInfo;
    }
}
