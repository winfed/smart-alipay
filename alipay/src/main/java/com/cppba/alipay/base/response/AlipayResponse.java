package com.cppba.alipay.base.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.cppba.alipay.base.request.AlipayRequest;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * 支付宝相应父类
 *
 * @author winfed
 * @create 2017-10-18 17:24
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlipayResponse {

    @JsonIgnore
    private AlipayRequest request;

    /**
     * https://docs.open.alipay.com/common/105806
     * 网关返回码
     */
    @JsonProperty("code")
    private String code;

    /**
     * 网关返回码描述
     */
    @JsonProperty("msg")
    private String msg;

    /**
     * 业务返回码，参见具体的API接口文档
     */
    @JsonProperty("sub_code")
    private String subCode;

    /**
     * 业务返回码描述，参见具体的API接口文档
     */
    @JsonProperty("sub_msg")
    private String subMsg;

    public boolean isSuccess() {
        return StringUtils.isNotBlank(this.code) && StringUtils.equals(this.code, "10000");
    }

    @JsonIgnore
    public String getServiceErrorLog() {
        return String.format("调用接口:[%s -> %s]成功，但返回业务失败: code=[%s],msg=[%s],subCode=[%s],subMsg=[%s]", request.getMethodName(), request.getMethod(), this.getCode(), this.getMsg(), this.getSubCode(), this.getSubMsg());
    }
}
