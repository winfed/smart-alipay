package com.cppba.alipay.base.exception;

/**
 * 支付宝接口通用异常
 *
 * @author winfed
 * @create 2017-10-18 17:14
 */
public class AlipayException extends Exception {

    public AlipayException(String code, String message) {
        super(String.format("%s: %s", code, message));
    }

    public AlipayException(String message) {
        super(message);
    }
}
