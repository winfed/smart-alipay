package com.cppba.alipay.response;

import com.cppba.alipay.base.response.AlipayResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.annotation.Generated;

/**
 * 单笔转账到支付宝账户接口
 * 接口名称：alipay.fund.trans.toaccount.transfer
 * url https://docs.open.alipay.com/api_28/alipay.fund.trans.toaccount.transfer
 * @author winfed
 * @create 2018-11-9 16:57
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlipayFundTransToaccountTransferResponse extends AlipayResponse {

	@JsonProperty("pay_date")
	private String payDate;

	@JsonProperty("out_biz_no")
	private String outBizNo;

	@JsonProperty("order_id")
	private String orderId;

}