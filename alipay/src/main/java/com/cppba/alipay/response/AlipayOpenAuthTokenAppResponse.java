package com.cppba.alipay.response;

import com.cppba.alipay.base.response.AlipayResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 使用app_auth_code换取app_auth_token
 * 接口名称：alipay.open.auth.token.app
 *
 * @author winfed
 * @create 2017-10-18 16:35
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlipayOpenAuthTokenAppResponse extends AlipayResponse {

    /**
     * 通过该令牌来帮助商户发起请求，完成业务
     * 201510BBaabdb44d8fd04607abf8d5931ec75D84
     */
    @JsonProperty("app_auth_token")
    private String appAuthToken;

    /**
     * 授权者的PID
     * 2088011177545623
     */
    @JsonProperty("user_id")
    private String userId;

    /**
     * 授权商户的AppId（如果有服务窗，则为服务窗的AppId）
     * 2013111800001989
     */
    @JsonProperty("auth_app_id")
    private String authAppId;

    /**
     * 交换令牌的有效期，单位秒，换算成天的话为365天
     * 31536000
     */
    @JsonProperty("expires_in")
    private Long expiresIn;

    /**
     * 刷新令牌有效期，单位秒，换算成天的话为372天
     * 32140800
     */
    @JsonProperty("re_expires_in")
    private Long reExpiresIn;

    /**
     * 刷新令牌后，我们会保证老的app_auth_token从刷新开始10分钟内可继续使用，请及时替换为最新token
     * 201510BB09dece3ea7654531b66bf9f97cdceE67
     */
    @JsonProperty("app_refresh_token")
    private String appRefreshToken;

}
