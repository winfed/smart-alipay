package com.cppba.alipay.response;

import java.util.Date;
import java.util.List;

import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.cppba.alipay.base.response.AlipayResponse;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.annotation.Generated;

/**
 * 统一收单交易退款接口
 * 接口名称：alipay.trade.refund
 *
 * @author winfed
 * @create 2017-11-21 9:57
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlipayTradeRefundResponse extends AlipayResponse {

    /**
     * 支付宝交易号
     * 2013112011001004330000121536
     */
    @JsonProperty("trade_no")
    private String tradeNo;

    /**
     * 商家订单号
     * 6823789339978248
     */
    @JsonProperty("out_trade_no")
    private String outTradeNo;

    /**
     * 用户的登录id
     * 159****5620
     */
    @JsonProperty("buyer_logon_id")
    private String buyerLogonId;

    /**
     * 本次退款是否发生了资金变化
     * Y
     */
    @JsonProperty("fund_change")
    private String fundChange;

    /**
     * 退款总金额
     * 88.88
     */
    @JsonProperty("refund_fee")
    private double refundFee;

    /**
     * 退款支付时间
     * 2014-11-27 15:45:57
     */
    @JsonProperty("gmt_refund_pay")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtRefundPay;

    /**
     * 退款使用的资金渠道
     */
    @JsonProperty("refund_detail_item_list")
    private List<RefundDetailItemListItem> refundDetailItemList;

    /**
     * 交易在支付时候的门店名称
     * 望湘园联洋店
     */
    @JsonProperty("store_name")
    private String storeName;

    /**
     * 买家在支付宝的用户id
     * 2088101117955611
     */
    @JsonProperty("buyer_user_id")
    private String buyerUserId;

    /**
     * 退款使用的资金渠道
     */
    public class RefundDetailItemListItem {

        /**
         * 交易使用的资金渠道
         * ALIPAYACCOUNT
         */
        @JsonProperty("real_amount")
        private String realAmount;

        /**
         * 该支付工具类型所使用的金额
         * 10
         */
        @JsonProperty("amount")
        private String amount;

        /**
         * 渠道实际付款金额
         * 11.21
         */
        @JsonProperty("fund_type")
        private String fundType;

        /**
         * 渠道所使用的资金类型,目前只在资金渠道(fund_channel)是银行卡渠道(BANKCARD)的情况下才返回该信息(DEBIT_CARD:借记卡,CREDIT_CARD:信用卡,MIXED_CARD:借贷合一卡)
         * DEBIT_CARD
         */
        @JsonProperty("fund_channel")
        private String fundChannel;
    }
}