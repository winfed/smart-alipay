package com.cppba.alipay.response;

import com.cppba.alipay.base.response.AlipayResponse;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

/**
 * 统一收单线下交易查询
 * 接口名称：alipay.trade.query
 *
 * @author winfed
 * @create 2017-10-18 16:35
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlipayTradeQueryResponse extends AlipayResponse {

    /**
     * 支付宝交易号
     * 2013112011001004330000121536
     */
    @JsonProperty("trade_no")
    private String tradeNo;

    /**
     * 商家订单号
     * 6823789339978248
     */
    @JsonProperty("out_trade_no")
    private String outTradeNo;

    /**
     * 买家支付宝账号
     * 159****5620
     */
    @JsonProperty("buyer_logon_id")
    private String buyerLogonId;

    /**
     * 交易状态：WAIT_BUYER_PAY（交易创建，等待买家付款）、TRADE_CLOSED（未付款交易超时关闭，或支付完成后全额退款）、TRADE_SUCCESS（交易支付成功）、TRADE_FINISHED（交易结束，不可退款）
     * TRADE_CLOSED
     */
    @JsonProperty("trade_status")
    private String tradeStatus;

    /**
     * 交易的订单金额，单位为元，两位小数。该参数的值为支付时传入的total_amount
     * 88.88
     */
    @JsonProperty("total_amount")
    private String totalAmount;

    /**
     * 实收金额，单位为元，两位小数。该金额为本笔交易，商户账户能够实际收到的金额
     * 15.25
     */
    @JsonProperty("receipt_amount")
    private String receiptAmount;

    /**
     * 买家实付金额，单位为元，两位小数。该金额代表该笔交易买家实际支付的金额，不包含商户折扣等金额
     * 8.88
     */
    @JsonProperty("buyer_pay_amount")
    private String buyerPayAmount;

    /**
     * 积分支付的金额，单位为元，两位小数。该金额代表该笔交易中用户使用积分支付的金额，比如集分宝或者支付宝实时优惠等
     * 10
     */
    @JsonProperty("point_amount")
    private String poinAmount;

    /**
     * 交易中用户支付的可开具发票的金额，单位为元，两位小数。该金额代表该笔交易中可以给用户开具发票的金额
     * 12.11
     */
    @JsonProperty("invoice_amount")
    private String invoiceAmount;

    /**
     * 本次交易打款给卖家的时间
     * 2014-11-27 15:45:57
     */
    @JsonProperty("send_pay_date")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date sendPayDate;

    /**
     * 商户门店编号
     * NJ_S_001
     */
    @JsonProperty("store_id")
    private String storeId;

    /**
     * 商户机具终端编号
     * NJ_T_001
     */
    @JsonProperty("terminal_id")
    private String terminalId;

    /**
     * 交易支付使用的资金渠道
     * [
     * {
     * "fund_channel": "ALIPAYACCOUNT",
     * "amount": 10,
     * "real_amount": 11.21,
     * "fund_type": "DEBIT_CARD"
     * }
     * ]
     */
    @JsonProperty("fund_bill_list")
    private FundBillList fundBillList;

    /**
     * 请求交易支付中的商户店铺的名称
     * 证大五道口店
     */
    @JsonProperty("store_name")
    private String storeName;

    /**
     * 买家在支付宝的用户id
     * 2088101117955611
     */
    @JsonProperty("buyer_user_id")
    private String buyerUserId;

    public class FundBillList{

        /**
         * 交易使用的资金渠道,详见https://doc.open.alipay.com/doc2/detail?treeId=26&articleId=103259&docType=1
         * ALIPAYACCOUNT
         */
        @JsonProperty("fund_channel")
        private String fundChannel;

        /**
         * 该支付工具类型所使用的金额
         * 10
         */
        @JsonProperty("amount")
        private String amount;

        /**
         * 渠道实际付款金额
         * 11.21
         */
        @JsonProperty("real_amount")
        private String realAmount;

        /**
         * 渠道所使用的资金类型,目前只在资金渠道(fund_channel)是银行卡渠道(BANKCARD)的情况下才返回该信息(DEBIT_CARD:借记卡,CREDIT_CARD:信用卡,MIXED_CARD:借贷合一卡)
         * DEBIT_CARD
         */
        @JsonProperty("fund_type")
        private String fundType;
    }
}
