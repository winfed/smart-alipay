package com.cppba.alipay.response;

import com.cppba.alipay.base.response.AlipayResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 使用app_auth_code换取app_auth_token
 * 接口名称：alipay.open.auth.token.app
 *
 * @author winfed
 * @create 2017-10-18 16:35
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlipayTradePrecreateResponse extends AlipayResponse {

    /**
     * 商户的订单号
     * 6823789339978248
     */
    @JsonProperty("out_trade_no")
    private String outTradeNo;

    /**
     * 当前预下单请求生成的二维码码串
     * https://qr.alipay.com/bavh4wjlxf12tper3a
     */
    @JsonProperty("qr_code")
    private String qrCode;
}
