package com.cppba.alipay.request;

import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.cppba.alipay.base.enums.SignEnum;
import com.cppba.alipay.base.exception.AlipayException;
import com.cppba.alipay.base.request.AlipayRequest;
import com.cppba.alipay.response.AlipayOpenAuthTokenAppQueryResponse;

/**
 * 查询授权信息
 * 接口名称：alipay.open.auth.token.app.query
 *
 * @author winfed
 * @create 2017-10-18 16:35
 */
public class AlipayOpenAuthTokenAppQueryRequest extends AlipayRequest<AlipayOpenAuthTokenAppQueryResponse> {

    /**
     * @param appId            应用ID
     * @param privateKey       应用私钥
     * @param alipayPublicKey  支付宝公钥
     * @param alipayBizContent 商户授权令牌
     */
    public AlipayOpenAuthTokenAppQueryRequest(String appId, String privateKey, String alipayPublicKey, SignEnum signType, AlipayBizContent alipayBizContent) {
        this(false, appId, privateKey, alipayPublicKey, signType, alipayBizContent);
    }

    /**
     * @param isSandbox        是否沙箱模式
     * @param appId            应用ID
     * @param privateKey       应用私钥
     * @param alipayPublicKey  支付宝公钥
     * @param alipayBizContent 商户授权令牌
     */
    public AlipayOpenAuthTokenAppQueryRequest(Boolean isSandbox, String appId, String privateKey, String alipayPublicKey, SignEnum signType, AlipayBizContent alipayBizContent) {
        super(isSandbox, "alipay.open.auth.token.app.query", appId, privateKey, alipayPublicKey, signType, alipayBizContent);
        super.methodName = "查询授权信息";
        super.clazz = AlipayOpenAuthTokenAppQueryResponse.class;
    }

    /**
     * 构建请求返回对象
     *
     * @return
     * @throws AlipayException
     */
    @Override
    public AlipayOpenAuthTokenAppQueryResponse createResponse() throws AlipayException {
        return sendRequest();
    }
}
