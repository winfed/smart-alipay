package com.cppba.alipay.request;

import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.cppba.alipay.base.enums.SignEnum;
import com.cppba.alipay.base.exception.AlipayException;
import com.cppba.alipay.base.request.AlipayRequest;
import com.cppba.alipay.response.AlipayDataDataserviceBillDownloadurlQueryResponse;
import com.cppba.alipay.response.AlipayFundTransToaccountTransferResponse;

/**
 * 查询对账单下载地址
 * 接口名称：alipay.data.dataservice.bill.downloadurl.query
 * url https://docs.open.alipay.com/api_15/alipay.data.dataservice.bill.downloadurl.query
 * @author winfed
 * @create 2018-11-10 16:57
 */
public class AlipayDataDataserviceBillDownloadurlQueryRequest extends AlipayRequest<AlipayDataDataserviceBillDownloadurlQueryResponse> {
	/**
	 * @param appId            应用ID
	 * @param privateKey       应用私钥
	 * @param alipayPublicKey  支付宝公钥
	 * @param alipayBizContent 商户授权令牌
	 */
	public AlipayDataDataserviceBillDownloadurlQueryRequest(String appId, String privateKey, String alipayPublicKey, SignEnum signType, AlipayBizContent alipayBizContent) {
		this(false, appId, privateKey, alipayPublicKey, signType, alipayBizContent);
	}

	/**
	 * @param isSandbox        是否沙箱模式
	 * @param appId            应用ID
	 * @param privateKey       应用私钥
	 * @param alipayPublicKey  支付宝公钥
	 * @param alipayBizContent 商户授权令牌
	 */
	public AlipayDataDataserviceBillDownloadurlQueryRequest(Boolean isSandbox, String appId, String privateKey, String alipayPublicKey, SignEnum signType, AlipayBizContent alipayBizContent) {
		super(isSandbox, "alipay.data.dataservice.bill.downloadurl.query", appId, privateKey, alipayPublicKey, signType, alipayBizContent);
		super.methodName = "查询对账单下载地址";
		super.clazz = AlipayDataDataserviceBillDownloadurlQueryResponse.class;
	}

	/**
	 * 构建请求返回对象
	 *
	 * @return
	 * @throws AlipayException
	 */
	@Override
	public AlipayDataDataserviceBillDownloadurlQueryResponse createResponse() throws AlipayException {
		return sendRequest();
	}
}