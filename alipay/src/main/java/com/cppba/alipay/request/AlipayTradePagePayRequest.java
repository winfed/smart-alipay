package com.cppba.alipay.request;


import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.cppba.alipay.base.enums.SignEnum;
import com.cppba.alipay.base.request.AlipayRequest;
import com.cppba.alipay.base.response.AlipayResponse;
import com.cppba.alipay.util.AlipayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * 统一收单下单并支付页面接口
 * 接口名称：alipay.trade.page.pay
 *
 * @author winfed
 * @create 2017-10-18 16:35
 */
public class AlipayTradePagePayRequest extends AlipayRequest<AlipayResponse> {

    /**
     * @param appId            应用ID
     * @param privateKey       应用私钥
     * @param alipayPublicKey  支付宝公钥
     * @param alipayBizContent 业务请求参数的集合
     * @param notifyUrl        异步回调
     * @param returnUrl        同步回调
     */
    public AlipayTradePagePayRequest(String appId, String privateKey, String alipayPublicKey, SignEnum signType, AlipayBizContent alipayBizContent, String notifyUrl, String returnUrl) {
        this(false, appId, privateKey, alipayPublicKey, signType, alipayBizContent, notifyUrl, returnUrl);
    }

    /**
     * @param isSandbox        是否沙箱模式
     * @param appId            应用ID
     * @param privateKey       应用私钥
     * @param alipayPublicKey  支付宝公钥
     * @param alipayBizContent 业务请求参数的集合
     * @param notifyUrl        异步回调
     * @param returnUrl        同步回调
     */
    public AlipayTradePagePayRequest(Boolean isSandbox, String appId, String privateKey, String alipayPublicKey, SignEnum signType, AlipayBizContent alipayBizContent, String notifyUrl, String returnUrl) {
        /////////////////////////////////////
        super(isSandbox, "alipay.trade.page.pay", appId, privateKey, alipayPublicKey, signType, alipayBizContent);
        super.methodName = "PC场景下单";
        super.clazz = AlipayResponse.class;
        // 公共参数
        if (StringUtils.isNotBlank(notifyUrl)) {
            this.addParameter("notify_url", notifyUrl);
        }
        // 同步回调
        if (StringUtils.isNotBlank(returnUrl)) {
            this.addParameter("return_url", returnUrl);
        }
    }

    public String createResposeHtml() {
        Map<String, Object> parameters = this.getPreRequestParameters();
        return AlipayUtils.buildRequest(this.getUrl(), parameters, "post");
    }
}
