package com.cppba.alipay.request;


import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.cppba.alipay.base.enums.SignEnum;
import com.cppba.alipay.base.exception.AlipayException;
import com.cppba.alipay.base.request.AlipayRequest;
import com.cppba.alipay.response.AlipayTradeQueryResponse;

/**
 * 统一收单线下交易查询
 * 接口名称：alipay.trade.query
 *
 * @author winfed
 * @create 2017-10-23 15:35
 */
public class AlipayTradeQueryRequest extends AlipayRequest<AlipayTradeQueryResponse> {

    /**
     * @param appId            应用ID
     * @param privateKey       应用私钥
     * @param alipayPublicKey  支付宝公钥
     * @param alipayBizContent 商户授权令牌
     */
    public AlipayTradeQueryRequest(String appId, String privateKey, String alipayPublicKey, SignEnum signType, AlipayBizContent alipayBizContent) {
        this(false, appId, privateKey, alipayPublicKey, signType, alipayBizContent);
    }

    /**
     * @param isSandbox        是否沙箱模式
     * @param appId            应用ID
     * @param privateKey       应用私钥
     * @param alipayPublicKey  支付宝公钥
     * @param alipayBizContent 商户授权令牌
     */
    public AlipayTradeQueryRequest(Boolean isSandbox, String appId, String privateKey, String alipayPublicKey, SignEnum signType, AlipayBizContent alipayBizContent) {
        super(isSandbox, "alipay.trade.query", appId, privateKey, alipayPublicKey, signType, alipayBizContent);
        super.methodName = "统一收单线下交易查询";
        super.clazz = AlipayTradeQueryResponse.class;
    }

    /**
     * 构建请求返回对象
     *
     * @return
     * @throws AlipayException
     */
    @Override
    public AlipayTradeQueryResponse createResponse() throws AlipayException {
        return sendRequest();
    }
}
