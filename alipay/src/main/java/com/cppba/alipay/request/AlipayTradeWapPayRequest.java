package com.cppba.alipay.request;


import com.cppba.alipay.base.bizcontent.AlipayBizContent;
import com.cppba.alipay.base.enums.SignEnum;
import com.cppba.alipay.base.request.AlipayRequest;
import com.cppba.alipay.base.response.AlipayResponse;
import com.cppba.alipay.util.AlipayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * 手机网站支付
 * 接口名称：alipay.trade.wap.pay
 *
 * @author winfed
 * @create 2017-10-18 16:35
 */
public class AlipayTradeWapPayRequest extends AlipayRequest<AlipayResponse> {

    /**
     * @param appId            应用ID
     * @param privateKey       应用私钥
     * @param alipayPublicKey  支付宝公钥
     * @param alipayBizContent 业务请求参数的集合
     * @param notifyUrl        异步回调
     */
    public AlipayTradeWapPayRequest(String appId, String privateKey, String alipayPublicKey, SignEnum signTye, AlipayBizContent alipayBizContent, String notifyUrl, String returnUrl) {
        this(false, appId, privateKey, alipayPublicKey, signTye, alipayBizContent, notifyUrl, returnUrl);
    }

    /**
     * @param isSandbox        是否沙箱模式
     * @param appId            应用ID
     * @param privateKey       应用私钥
     * @param alipayPublicKey  支付宝公钥
     * @param alipayBizContent 业务请求参数的集合
     * @param notifyUrl        异步回调
     */
    public AlipayTradeWapPayRequest(Boolean isSandbox, String appId, String privateKey, String alipayPublicKey, SignEnum signTye, AlipayBizContent alipayBizContent, String notifyUrl, String returnUrl) {
        /////////////////////////////////////
        super(isSandbox, "alipay.trade.wap.pay", appId, privateKey, alipayPublicKey, signTye, alipayBizContent);
        super.methodName = "手机网站支付";
        super.clazz = AlipayResponse.class;
        // 公共参数
        if (StringUtils.isNotBlank(notifyUrl)) {
            this.addParameter("notify_url", notifyUrl);
        }
        // 同步回调
        if (StringUtils.isNotBlank(returnUrl)) {
            this.addParameter("return_url", returnUrl);
        }
    }

    /**
     * 返回给H5页面
     *
     * @return
     */
    public String createResposeHtml() {
        Map<String, Object> parameters = this.getPreRequestParameters();
        return AlipayUtils.buildRequest(this.getUrl(), parameters, "post");
    }
}
