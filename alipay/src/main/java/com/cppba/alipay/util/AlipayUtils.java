package com.cppba.alipay.util;

import java.util.Map;

/**
 * @author winfed
 * @create 2017-10-20 11:45
 */
public class AlipayUtils {
    /**
     * @param baseUrl   请求地址
     * @param orderInfo 发起支付的订单信息
     * @param method    请求方式 "post" "get",
     * @return 获取输出消息，用户返回给支付端, 针对于web端
     */
    public static String buildRequest(String baseUrl, Map<String, Object> orderInfo, String method) {
        StringBuffer formHtml = new StringBuffer();
        formHtml.append("<form id=\"_alipaysubmit_\" name=\"alipaysubmit\" action=\"");
        String biz_content = (String) orderInfo.get("biz_content");
        formHtml.append(baseUrl).append("?").append(UriVariableUtils.getMapToParameters(orderInfo)).append("\" method=\"").append(method).append("\">");
        formHtml.append("<input type=\"hidden\" name=\"biz_content\" value=\'" + biz_content + "\'/>");
        formHtml.append("</form>");
        formHtml.append("<script>document.forms['_alipaysubmit_'].submit();</script>");

        return formHtml.toString();
    }
}
