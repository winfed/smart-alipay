package com.cppba.alipay.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.UUID;

/**
 * @author winfed
 * @create 2017-10-18 17:19
 */
public class SignUtils {


    /**
     * 把数组所有元素排序，并按照“参数=参数值”的模式用“@param separator”字符拼接成字符串
     *
     * @param parameters 参数
     * @return 去掉空值与签名参数后的新签名，拼接后字符串
     */
    public static String parameterText(Map parameters) {
        return parameterText(parameters, "&");

    }

    /**
     * 把数组所有元素排序，并按照“参数=参数值”的模式用“@param separator”字符拼接成字符串
     *
     * @param parameters 参数
     * @param separator  分隔符
     * @return 去掉空值与签名参数后的新签名，拼接后字符串
     */
    public static String parameterText(Map parameters, String separator) {
        return parameterText(parameters, separator, "sign", "sign_type");
    }

    /**
     * 把数组所有元素排序，并按照“参数=参数值”的模式用“@param separator”字符拼接成字符串
     *
     * @param parameters 参数
     * @param separator  分隔符
     * @param ignoreKey  需要忽略添加的key
     * @return 去掉空值与签名参数后的新签名，拼接后字符串
     */
    @SuppressWarnings("unchecked")
    public static String parameterText(Map parameters, String separator, String... ignoreKey) {
        if (parameters == null) {
            return "";
        }
        StringBuffer sb = new StringBuffer();
        if (null != ignoreKey) {
            Arrays.sort(ignoreKey);
        }
        if (parameters instanceof SortedMap) {
            Set<Map.Entry> set = parameters.entrySet();
            for (Map.Entry o : set) {
                Object v = o.getValue();
                if (null == v || "".equals(v.toString().trim()) || (null != ignoreKey && Arrays.binarySearch(ignoreKey, o.getKey()) >= 0)) {
                    continue;
                }
                sb.append(o.getKey()).append("=").append(v.toString().trim()).append(separator);
            }
            if (sb.length() > 0 && !"".equals(separator)) {
                sb.deleteCharAt(sb.length() - 1);
            }
            return sb.toString();

        }

        List<String> keys = new ArrayList<String>(parameters.keySet());
        // 排序
        Collections.sort(keys);
        for (String k : keys) {
            StringBuilder valueStr = new StringBuilder();
            Object o = parameters.get(k);
            if (o instanceof String[]) {
                String[] values = (String[]) o;
                for (int i = 0; i < values.length; i++) {
                    String value = values[i].trim();
                    if ("".equals(value)) {
                        continue;
                    }
                    valueStr.append((i == values.length - 1) ? value : value + ",");
                }
            } else if (o != null) {
                valueStr = new StringBuilder(o.toString());
            }
            if ("".equals(valueStr.toString().trim()) || null != ignoreKey && Arrays.binarySearch(ignoreKey, k) >= 0) {
                continue;
            }
            sb.append(k).append("=").append(valueStr).append(separator);
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    /**
     * 获取随机字符串
     *
     * @return 随机字符串
     */
    public static String randomStr() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
