package com.cppba.alipay.controller;

import com.cppba.alipay.bizcontent.AlipayTradeWapPayBizContent;
import com.cppba.alipay.properties.AlipayProperties;
import com.cppba.alipay.request.AlipayTradeWapPayRequest;
import com.cppba.alipay.util.MoneyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 统一收单下单并支付页面接口
 *
 * @author winfed
 * @create 2017-10-23 15:32
 */
@Controller
@RequestMapping("/alipay")
public class AlipayTradeWapPayController {

    private final AlipayProperties alipayProperties;

    @Autowired
    public AlipayTradeWapPayController(AlipayProperties alipayProperties) {
        this.alipayProperties = alipayProperties;
    }

    /**
     * 发起网站支付
     *
     * @return 跳到支付页面
     */
    @RequestMapping(value = "/trade/wap/pay", produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public String alipayTradeWapPay() {
        // http://127.0.0.1:8080/alipay/trade/wap/pay
        // 模拟数据
        String serialNumber = "123456";
        String title = "测试订单title";
        String body = "测试订单body";
        Long orderAmount = 1L;
        // 业务参数
        AlipayTradeWapPayBizContent alipayTradeWapPayBizContent = new AlipayTradeWapPayBizContent();
        alipayTradeWapPayBizContent.setOutTradeNo(serialNumber);
        alipayTradeWapPayBizContent.setBody(body);
        alipayTradeWapPayBizContent.setSubject(title);
        alipayTradeWapPayBizContent.setTotalAmount(MoneyUtils.getMoney(orderAmount).toString());
        alipayTradeWapPayBizContent.setProductCode("QUICK_WAP_WAY");
        alipayTradeWapPayBizContent.setTimeoutExpress(alipayProperties.getTimeoutExpress());
        // alipayTradeWapPayBizContent.setExtendParams(new AlipayTradeWapPayBizContent.ExtendParams());
        // 请求构建对象
        AlipayTradeWapPayRequest alipayTradeWapPayRequest = new AlipayTradeWapPayRequest(alipayProperties.getIsTest(), alipayProperties.getAppId(), alipayProperties.getPrivateKey(), alipayProperties.getAlipayPublicKey(),
                alipayProperties.getSignType(), alipayTradeWapPayBizContent, alipayProperties.getNotifyUrl(), null);
        // 构建html返回页面
        return alipayTradeWapPayRequest.createResposeHtml();
    }
}
