package com.cppba.alipay.controller;

import com.cppba.alipay.base.exception.AlipayException;
import com.cppba.alipay.bizcontent.AlipayTradePrecreateBizContent;
import com.cppba.alipay.properties.AlipayProperties;
import com.cppba.alipay.request.AlipayTradeRrecreateRequest;
import com.cppba.alipay.response.AlipayTradePrecreateResponse;
import com.cppba.alipay.util.MoneyUtils;
import com.cppba.alipay.util.QrCodeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 统一收单线下交易预创建
 *
 * @author winfed
 * @create 2017-10-23 15:32
 */
@Slf4j
@Controller
@RequestMapping("/alipay")
public class AlipayTradePrecreateController {

    private final AlipayProperties alipayProperties;

    @Autowired
    public AlipayTradePrecreateController(AlipayProperties alipayProperties) {
        this.alipayProperties = alipayProperties;
    }

    /**
     * 统一收单线下交易预创建
     *
     * @return 支付二维码
     */
    @RequestMapping(value = "/trade/precreate", produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public String alipayTradePrecreate() {
        // http://127.0.0.1:8080/alipay/trade/precreate
        // 模拟数据
        String serialNumber = "20171026123456";
        String title = "测试订单title";
        String body = "测试订单body";
        Long orderAmount = 1L;
        // 业务参数
        // 授权码,如果传了，表示替签约商户发起收单
        // String appAuthToken = "201710BB05e93d94029644488e3ec22a5b0dbX04";
        AlipayTradePrecreateBizContent alipayTradePrecreateBizContent = new AlipayTradePrecreateBizContent();
        alipayTradePrecreateBizContent.setOutTradeNo(serialNumber);
        alipayTradePrecreateBizContent.setBody(body);
        alipayTradePrecreateBizContent.setSubject(title);
        alipayTradePrecreateBizContent.setTotalAmount(MoneyUtils.getMoney(orderAmount).toString());
        alipayTradePrecreateBizContent.setTimeoutExpress(alipayProperties.getTimeoutExpress());
        // 请求构建对象
        AlipayTradeRrecreateRequest alipayTradeRrecreateRequest = new AlipayTradeRrecreateRequest(alipayProperties.getIsTest(), alipayProperties.getAppId(), alipayProperties.getPrivateKey(), alipayProperties.getAlipayPublicKey(),
                alipayProperties.getSignType(), alipayTradePrecreateBizContent, alipayProperties.getNotifyUrl(), null);
        try {
            // 发送请求，获取调用结果
            AlipayTradePrecreateResponse response = alipayTradeRrecreateRequest.createResponse();
            // 判断是否返回成功，若失败打印日志
            if (!response.isSuccess()) {
                log.error("{}", response.getServiceErrorLog());
                return "";
            }

            if (StringUtils.isNotBlank(response.getQrCode())) {
                String base64 = QrCodeUtils.bufferedImage2Base64(QrCodeUtils.writeInfoToJpgBuff(response.getQrCode()));
                return "<html>收款二维码:<br/><img src='data:image/png;base64," + base64 + "'/ ><br/> 收款订单号:" + response.getOutTradeNo() + "</html>";
            }

        } catch (AlipayException e) {
            log.error("", e);
        }
        return "";
    }
}
