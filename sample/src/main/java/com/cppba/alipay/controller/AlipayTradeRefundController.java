package com.cppba.alipay.controller;

import com.cppba.alipay.base.exception.AlipayException;
import com.cppba.alipay.base.response.AlipayResponse;
import com.cppba.alipay.bizcontent.AlipayTradeRefundBizContent;
import com.cppba.alipay.properties.AlipayProperties;
import com.cppba.alipay.request.AlipayTradeRefundRequest;
import com.cppba.alipay.response.AlipayTradeRefundResponse;
import com.cppba.alipay.util.MoneyUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

/**
 * 统一收单交易退款接口
 *
 * @author winfed
 * @create 2017-10-23 17:27
 */
@Slf4j
@Controller
@RequestMapping("/alipay")
public class AlipayTradeRefundController {

    private final AlipayProperties alipayProperties;

    @Autowired
    public AlipayTradeRefundController(AlipayProperties alipayProperties) {
        this.alipayProperties = alipayProperties;
    }

    /**
     * 交易查询
     *
     * @return 查询结果
     */
    @RequestMapping(value = "/trade/refund", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public AlipayResponse doTradeRefund(String outTradeNo) {
        // http://127.0.0.1:8080/alipay/trade/refund
        // 模拟数据
        outTradeNo = StringUtils.isBlank(outTradeNo) ? "123456789" : outTradeNo;
        Long orderAmount = 100L;
        // 业务参数
        AlipayTradeRefundBizContent alipayTradeRefundBizContent = new AlipayTradeRefundBizContent();
        alipayTradeRefundBizContent.setOutTradeNo(outTradeNo);
        alipayTradeRefundBizContent.setRefundAmount(MoneyUtils.getMoney(orderAmount).toString());
        alipayTradeRefundBizContent.setOutRequestNo(UUID.randomUUID().toString().replace("-", ""));
        // 请求构建对象
        AlipayTradeRefundRequest alipayTradeRefundRequest = new AlipayTradeRefundRequest(alipayProperties.getIsTest(),
                alipayProperties.getAppId(), alipayProperties.getPrivateKey(), alipayProperties.getAlipayPublicKey(),
                alipayProperties.getSignType(), alipayTradeRefundBizContent);
        try {
            // 发送请求，获取调用结果
            AlipayTradeRefundResponse response = alipayTradeRefundRequest.createResponse();
            // 判断是否返回成功，若失败打印日志
            if (!response.isSuccess()) {
                log.error("{}", response.getServiceErrorLog());
            }
            // 打印返回结果
            log.info(response.toString());
            return response;
        } catch (AlipayException e) {
            log.error("", e);
        }
        return null;
    }
}
