package com.cppba.alipay.controller;

import com.cppba.alipay.bizcontent.AlipayTradeAppPayBizContent;
import com.cppba.alipay.properties.AlipayProperties;
import com.cppba.alipay.request.AlipayTradeAppPayRequest;
import com.cppba.alipay.util.MoneyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * APP支付接口
 *
 * @author winfed
 * @create 2017-10-23 15:32
 */
@Controller
@RequestMapping("/alipay")
public class AlipayTradeAppPayController {

    private final AlipayProperties alipayProperties;

    @Autowired
    public AlipayTradeAppPayController(AlipayProperties alipayProperties) {
        this.alipayProperties = alipayProperties;
    }

    /**
     * 拼接app支付需要参数
     *
     * @return app支付参数
     */
    @RequestMapping(value = "/trade/app/pay", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String alipayTradeAppPay() {
        // http://127.0.0.1:8080/alipay/trade/app/pay
        // 模拟数据
        String serialNumber = "12345";
        String title = "测试订单title";
        String body = "测试订单body";
        Long orderAmount = 1L;
        // 业务参数
        AlipayTradeAppPayBizContent alipayTradeAppPayBizContent = new AlipayTradeAppPayBizContent();
        alipayTradeAppPayBizContent.setOutTradeNo(serialNumber);
        alipayTradeAppPayBizContent.setBody(body);
        alipayTradeAppPayBizContent.setSubject(title);
        alipayTradeAppPayBizContent.setTotalAmount(MoneyUtils.getMoney(orderAmount).toString());
        alipayTradeAppPayBizContent.setProductCode("QUICK_MSECURITY_PAY");
        alipayTradeAppPayBizContent.setTimeoutExpress(alipayProperties.getTimeoutExpress());
        // alipayTradePagePayBizContent.setExtendParams(new AlipayTradePagePayBizContent.ExtendParams());
        // 请求构建对象
        AlipayTradeAppPayRequest alipayTradeAppPayRequest = new AlipayTradeAppPayRequest(alipayProperties.getIsTest(), alipayProperties.getAppId(), alipayProperties.getPrivateKey(), alipayProperties.getAlipayPublicKey(),
                alipayProperties.getSignType(), alipayTradeAppPayBizContent, alipayProperties.getNotifyUrl());
        // 构建html返回页面
        return alipayTradeAppPayRequest.createResposeParameter();
    }
}
