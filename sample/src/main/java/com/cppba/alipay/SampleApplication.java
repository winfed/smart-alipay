package com.cppba.alipay;

import com.cppba.alipay.properties.AlipayProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author winfed
 * @create 2017-10-23 15:30
 */
@SpringBootApplication
@EnableConfigurationProperties(AlipayProperties.class)
public class SampleApplication {
    public static void main(String[] args) {
        SpringApplication.run(SampleApplication.class, args);
    }
}
